package cz.yusari.sb.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;

public class DropFixListener implements Listener {

    @EventHandler
    public void onDeath(EntityDeathEvent e) {
        if (e.getEntity().getType() == EntityType.SHEEP) {
            if (e.getDrops().isEmpty()) {
                if (e.getEntity().getLastDamageCause().getCause() == EntityDamageEvent.DamageCause.LAVA ||
                e.getEntity().getLastDamageCause().getCause() == EntityDamageEvent.DamageCause.FIRE) {
                    e.getEntity().getWorld().dropItemNaturally(e.getEntity().getLocation(), new ItemStack(Material.COOKED_MUTTON));
                } else {
                    e.getEntity().getWorld().dropItemNaturally(e.getEntity().getLocation(), new ItemStack(Material.MUTTON));
                }
                e.getEntity().getWorld().dropItemNaturally(e.getEntity().getLocation(), new ItemStack(Material.WHITE_WOOL));
                } else
                    e.getDrops().forEach(itemStack -> {
                        e.getEntity().getWorld().dropItemNaturally(e.getEntity().getLocation(), itemStack);
                    });
            }
        }

        @EventHandler
    public void onTeleport(PlayerTeleportEvent e ) {
        if (e.getTo().getWorld().getName().startsWith("world")) {
            if (e.getPlayer().getLocation().getWorld().getName().startsWith("world")) {
                Bukkit.dispatchCommand(e.getPlayer(), "spawn");
            }
            e.setCancelled(true);
            e.getPlayer().sendMessage("§cNelze se teleportovat do daného světa.");
        }
        }
    }
