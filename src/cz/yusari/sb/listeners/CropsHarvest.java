package cz.yusari.sb.listeners;

import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import net.minecraft.server.v1_15_R1.PacketPlayOutAnimation;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.Ageable;
import org.bukkit.craftbukkit.v1_15_R1.entity.CraftPlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class CropsHarvest implements Listener {

    @EventHandler
    public void onClick(PlayerInteractEvent e) {
        if (e.isCancelled()) {
            return;
        }
        if (e.getAction() == Action.LEFT_CLICK_BLOCK) {
            return;
        }
        if (e.getClickedBlock().getType() == Material.SWEET_BERRY_BUSH ||
        e.getClickedBlock().getType() == Material.MELON_STEM ||
        e.getClickedBlock().getType() == Material.PUMPKIN_STEM ||
        e.getClickedBlock().getType() == Material.BAMBOO ||
        e.getClickedBlock().getType() == Material.BAMBOO_SAPLING ||
        e.getClickedBlock().getType() == Material.CACTUS) {
            return;
        }
        AtomicReference<String> items = new AtomicReference<>("");
        if (e.getClickedBlock().getBlockData() instanceof Ageable) {
            Ageable age = (Ageable) e.getClickedBlock().getBlockData();
            if(age.getAge() == age.getMaximumAge()){
                e.getClickedBlock().getDrops().forEach(itemStack -> {
                    if (itemStack.getType().toString().toLowerCase().contains("seed")) {

                    } else {
                        e.getPlayer().getInventory().addItem(itemStack);
                        if (items.get() == "") {
                            items.set(itemStack.getAmount() + " " + toTitleCase(itemStack.getType().toString()));
                        } else
                            items.set(items.get() + ", " + itemStack.getAmount() + " " + toTitleCase(itemStack.getType().toString()));
                    }
                });
                PacketPlayOutAnimation packet = new PacketPlayOutAnimation(((CraftPlayer) e.getPlayer()).getHandle(), 0);
                ((CraftPlayer) e.getPlayer()).getHandle().playerConnection.sendPacket(packet);
                e.getClickedBlock().setType(e.getClickedBlock().getType());
                e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
                e.getPlayer().spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§8§m--§f §a§l+ §a" + items.get() + " §8§m--"));
            }
        }
    }
 /*   @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if (e.getAction() == Action.LEFT_CLICK_BLOCK) {
            if (e.getPlayer().getInventory().getItemInMainHand() != null) {
                if (e.getPlayer().getInventory().getItemInMainHand().hasItemMeta()) {
                    if (e.getPlayer().getInventory().getItemInMainHand().getItemMeta().hasDisplayName()) {
                        if (e.getPlayer().getInventory().getItemInMainHand().getItemMeta().getDisplayName().equals("§bCropper")) {
                            if (e.getPlayer().getInventory().getItemInMainHand().getItemMeta().hasLore()) {
                                if (e.getClickedBlock().getType() == Material.SWEET_BERRY_BUSH ||
                                        e.getClickedBlock().getType() == Material.MELON_STEM ||
                                        e.getClickedBlock().getType() == Material.PUMPKIN_STEM ||
                                        e.getClickedBlock().getType() == Material.BAMBOO ||
                                        e.getClickedBlock().getType() == Material.BAMBOO_SAPLING ||
                                        e.getClickedBlock().getType() == Material.CACTUS) {
                                    return;
                                }
                                if (e.getClickedBlock().getBlockData() instanceof Ageable) {
                                    Block block = e.getClickedBlock();
                                    List<Block> relatives = new ArrayList<Block>();
                                    for (BlockFace blockface : BlockFace.values()) {
                                        relatives.add(block.getRelative(blockface));
                                        Block ablock = block.getRelative(blockface);
                                        relatives.add(ablock.getRelative(blockface));
                                    }
                                    int durability = 0;
                                    for (Block blocks : relatives) {
                                        if (blocks.getBlockData() instanceof Ageable) {
                                            if (blocks.getType() == Material.SWEET_BERRY_BUSH ||
                                                    blocks.getType() == Material.MELON_STEM ||
                                                    blocks.getType() == Material.PUMPKIN_STEM ||
                                                    blocks.getType() == Material.BAMBOO ||
                                                    blocks.getType() == Material.BAMBOO_SAPLING ||
                                                    blocks.getType() == Material.CACTUS) {

                                            } else
                                                durability++;
                                            ItemStack item = e.getPlayer().getInventory().getItemInMainHand();
                                            Damageable meta = (Damageable) item.getItemMeta();
                                            if (durability > meta.getDamage()) {
                                                blocks.breakNaturally();
                                                blocks.setType(blocks.getType());
                                            } else
                                                e.getPlayer().getWorld().playSound(e.getPlayer().getLocation(), Sound.ENTITY_ITEM_BREAK, 1, 1);
                                            e.getPlayer().getInventory().setItemInMainHand(new ItemStack(Material.AIR));
                                            relatives.clear();
                                            return;
                                        }
                                    }
                                    ItemStack item = e.getPlayer().getInventory().getItemInMainHand();
                                    Damageable meta = (Damageable) item.getItemMeta();
                                    meta.setDamage(meta.getDamage() + durability);
                                    item.setItemMeta((ItemMeta) meta);
                                    ItemStack meta1 = new ItemStack(item.getType());
                                    if (meta1.getType().getMaxDurability() > meta.getDamage()) {
                                        e.getPlayer().getInventory().setItemInMainHand(item);
                                        relatives.clear();
                                    } else
                                        e.getPlayer().getWorld().playSound(e.getPlayer().getLocation(), Sound.ENTITY_ITEM_BREAK, 1, 1);
                                    e.getPlayer().getInventory().setItemInMainHand(new ItemStack(Material.AIR));
                                    Bukkit.broadcastMessage(durability + " " + meta1.getType().getMaxDurability() +  " > " + meta.getDamage());
                                    relatives.clear();
                                }
                            }
                            }
                        }
                    }
                }
            }
    }*/

    public static String toTitleCase(String givenString) {
        try {
            String[] arr = givenString.split(" ");
            StringBuffer sb = new StringBuffer();

            for (int i = 0; i < arr.length; i++) {
                sb.append(Character.toUpperCase(arr[i].charAt(0)))
                        .append(arr[i].substring(1)).append(" ");
            }
            return sb.toString().trim();
        } catch (Exception ex){
            return "?";
        }
    }
}
