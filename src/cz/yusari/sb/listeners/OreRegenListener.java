package cz.yusari.sb.listeners;

import cz.yusari.sb.Main;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.data.BlockData;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.concurrent.atomic.AtomicReference;

public class OreRegenListener implements Listener {

    @EventHandler(ignoreCancelled = true)
    public void onBreak(BlockBreakEvent e) {
        Block block = e.getBlock();
        Material mat = block.getType();
        BlockData data = block.getBlockData();
        if (e.getPlayer().getWorld().getName().equalsIgnoreCase("mines2")) {
            if (e.getBlock().getType().toString().toLowerCase().contains("ore") || e.getBlock().getType() == Material.GLOWSTONE) {
                e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
                e.getBlock().getDrops().forEach(item -> {e.getPlayer().getInventory().addItem(item);});
                e.getBlock().setType(Material.BEDROCK);
                new BukkitRunnable() {
                    public void run() {
                        if (mat == Material.CHORUS_PLANT) {
                            block.setType(Material.CHORUS_FLOWER);
                        } else {
                            block.setType(mat);
                            block.setBlockData(data);
                        }
                        block.getWorld().playSound(block.getLocation(), Sound.BLOCK_STONE_BREAK, 1, 2);
                        block.getWorld().spawnParticle(Particle.PORTAL, block.getLocation().getX(), block.getLocation().getY(), block.getLocation().getZ(), 100, 1, 1, 1, 0);
                    }
                }.runTaskLater(Main.getInstance(), 45 * 20);
            }
        }
    }
    public static String toTitleCase(String givenString) {
        try {
            String[] arr = givenString.split(" ");
            StringBuffer sb = new StringBuffer();

            for (int i = 0; i < arr.length; i++) {
                sb.append(Character.toUpperCase(arr[i].charAt(0)))
                        .append(arr[i].substring(1)).append(" ");
            }
            return sb.toString().trim();
        } catch (Exception ex){
            return "?";
        }
    }
}
