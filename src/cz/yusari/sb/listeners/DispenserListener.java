package cz.yusari.sb.listeners;

import com.google.common.collect.Multimap;
import cz.yusari.sb.Main;
import net.minecraft.server.v1_15_R1.AttributeModifier;
import net.minecraft.server.v1_15_R1.EnumItemSlot;
import net.minecraft.server.v1_15_R1.GenericAttributes;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.Container;
import org.bukkit.block.Dispenser;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Directional;
import org.bukkit.craftbukkit.v1_15_R1.inventory.CraftItemStack;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockDispenseEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Collection;
import java.util.UUID;

public class DispenserListener implements Listener {

    @EventHandler
    public void onDispense(BlockDispenseEvent e) {
        Block block = e.getBlock();
        if (block != null && block.getState() instanceof Dispenser) {
            BlockData data = block.getBlockData();
            if (data instanceof Directional) {
                Directional directional = (Directional) data;
                Container container = (Container) block.getState();
                Block blockNext = block.getRelative(directional.getFacing());
                if (!blockNext.getType().toString().toLowerCase().contains("ore") || blockNext.getType() != Material.OBSIDIAN ||
                blockNext.getType() != Material.COBBLESTONE || blockNext.getType() != Material.STONE) {
                    return;
                }
                /*                MINING DISPENSER                    */
                if (e.getItem().getType() == Material.WOODEN_PICKAXE) {
                    e.setCancelled(true);
                    for(ItemStack it : container.getInventory().getContents())
                    {
                        if(it != null) {
                            block.getWorld().getNearbyEntities(block.getLocation(), 10, 10, 10).forEach(entity -> {
                                if (entity instanceof Player) {
                                    ((Player) entity).sendMessage("§3WarfareDispenser §8» §7Dispenser na souřadnicích §fX " + block.getLocation().getX() + " Y "
                                            + block.getLocation().getY() + " Z " + block.getLocation().getZ() + " §7obsahuje více jak jeden Pickaxe. V tomto Dispenseru může být jen jeden Pickaxe.");
                                }
                            });
                            return;
                        }
                    }
                    if (blockNext.getType() == Material.DIAMOND_ORE ||
                            blockNext.getType() == Material.REDSTONE_ORE ||
                            blockNext.getType() == Material.LAPIS_ORE ||
                            blockNext.getType() == Material.OBSIDIAN ||
                            blockNext.getType() == Material.GOLD_ORE ||
                            blockNext.getType() == Material.IRON_ORE ||
                            blockNext.getType() == Material.EMERALD_ORE ||
                            blockNext.getType() == Material.NETHER_QUARTZ_ORE ||
                            blockNext.getType() == Material.BEDROCK) {
                        if (!(blockNext.getType().isSolid())) {
                            block.getWorld().playSound(block.getLocation(), Sound.BLOCK_ANVIL_FALL, 1, 1);
                        }
                    } else
                        blockNext.breakNaturally();
                    Bukkit.getServer().getScheduler().runTaskLater(Main.getInstance(), () -> { ItemStack item = e.getItem();
                        Damageable meta = (Damageable) e.getItem().getItemMeta();
                        meta.setDamage(meta.getDamage() + 1);
                        item.setItemMeta((ItemMeta) meta);
                        for(int i = 0; i < 9; i++) {
                            container.getInventory().setItem(i, new ItemStack(Material.AIR));
                        }
                        ItemStack meta1 = new ItemStack(Material.WOODEN_PICKAXE);
                        if (meta1.getType().getMaxDurability() > meta.getDamage()) {
                            container.getInventory().setItem(0, item);
                        } else
                            block.getWorld().getNearbyEntities(block.getLocation(), 10, 10, 10).forEach(entity -> {
                                if (entity instanceof Player) {
                                    ((Player) entity).sendMessage("§3WarfareDispenser §8» §7Item v Dispenseru §fX " + block.getLocation().getX() + " Y "
                                            + block.getLocation().getY() + " Z " + block.getLocation().getZ() + " §7se rozbil.");
                                }
                            });
                    }, 1);

                }
                if (e.getItem().getType() == Material.STONE_PICKAXE) {
                    e.setCancelled(true);
                    for(ItemStack it : container.getInventory().getContents())
                    {
                        if(it != null) {
                            block.getWorld().getNearbyEntities(block.getLocation(), 10, 10, 10).forEach(entity -> {
                                if (entity instanceof Player) {
                                    ((Player) entity).sendMessage("§3WarfareDispenser §8» §7Dispenser na souřadnicích §fX " + block.getLocation().getX() + " Y "
                                            + block.getLocation().getY() + " Z " + block.getLocation().getZ() + " §7obsahuje více jak jeden Pickaxe. V tomto Dispenseru může být jen jeden Pickaxe.");
                                }
                            });
                            return;
                        }
                    }
                    if (blockNext.getType() != Material.DIAMOND_ORE ||
                            blockNext.getType() != Material.REDSTONE_ORE ||
                            blockNext.getType() != Material.OBSIDIAN ||
                            blockNext.getType() != Material.GOLD_ORE ||
                            blockNext.getType() != Material.EMERALD_ORE ||
                            blockNext.getType() != Material.BEDROCK) {
                        if (blockNext.getType().isSolid()) {
                            blockNext.breakNaturally();
                            Bukkit.getServer().getScheduler().runTaskLater(Main.getInstance(), () -> {
                                ItemStack item = e.getItem();
                                Damageable meta = (Damageable) e.getItem().getItemMeta();
                                meta.setDamage(meta.getDamage() + 1);
                                item.setItemMeta((ItemMeta) meta);
                                for (int i = 0; i < 9; i++) {
                                    container.getInventory().setItem(i, new ItemStack(Material.AIR));
                                }
                                ItemStack meta1 = new ItemStack(Material.STONE_PICKAXE);
                                if (meta1.getType().getMaxDurability() > meta.getDamage()) {
                                    container.getInventory().setItem(0, item);
                                } else
                                    block.getWorld().getNearbyEntities(block.getLocation(), 10, 10, 10).forEach(entity -> {
                                        if (entity instanceof Player) {
                                            ((Player) entity).sendMessage("§3WarfareDispenser §8» §7Item v Dispenseru §fX " + block.getLocation().getX() + " Y "
                                                    + block.getLocation().getY() + " Z " + block.getLocation().getZ() + " §7se rozbil.");
                                        }
                                    });
                            }, 1);
                        }
                    } else
                        block.getWorld().playSound(block.getLocation(), Sound.BLOCK_ANVIL_FALL, 1, 1);
                }
                if (e.getItem().getType() == Material.IRON_PICKAXE) {
                    e.setCancelled(true);
                    for(ItemStack it : container.getInventory().getContents())
                    {
                        if(it != null) {
                            block.getWorld().getNearbyEntities(block.getLocation(), 10, 10, 10).forEach(entity -> {
                                if (entity instanceof Player) {
                                    ((Player) entity).sendMessage("§3WarfareDispenser §8» §7Dispenser na souřadnicích §fX " + block.getLocation().getX() + " Y "
                                            + block.getLocation().getY() + " Z " + block.getLocation().getZ() + " §7obsahuje více jak jeden Pickaxe. V tomto Dispenseru může být jen jeden Pickaxe.");
                                }
                            });
                            return;
                        }
                    }
                    if (blockNext.getType() != Material.OBSIDIAN ||
                            blockNext.getType() != Material.BEDROCK) {
                        if (blockNext.getType().isSolid()) {
                            blockNext.breakNaturally();
                            Bukkit.getServer().getScheduler().runTaskLater(Main.getInstance(), () -> {
                                ItemStack item = e.getItem();
                                Damageable meta = (Damageable) e.getItem().getItemMeta();
                                meta.setDamage(meta.getDamage() + 1);
                                item.setItemMeta((ItemMeta) meta);
                                for (int i = 0; i < 9; i++) {
                                    container.getInventory().setItem(i, new ItemStack(Material.AIR));
                                }
                                ItemStack meta1 = new ItemStack(Material.IRON_PICKAXE);
                                if (meta1.getType().getMaxDurability() > meta.getDamage()) {
                                    container.getInventory().setItem(0, item);
                                } else
                                    block.getWorld().getNearbyEntities(block.getLocation(), 10, 10, 10).forEach(entity -> {
                                        if (entity instanceof Player) {
                                            ((Player) entity).sendMessage("§3WarfareDispenser §8» §7Item v Dispenseru §fX " + block.getLocation().getX() + " Y "
                                                    + block.getLocation().getY() + " Z " + block.getLocation().getZ() + " §7se rozbil.");
                                        }
                                    });
                            }, 1);
                        }
                    } else
                        block.getWorld().playSound(block.getLocation(), Sound.BLOCK_ANVIL_FALL, 1, 1);
                }
                if (e.getItem().getType() == Material.DIAMOND_PICKAXE) {
                    e.setCancelled(true);
                    for(ItemStack it : container.getInventory().getContents())
                    {
                        if(it != null) {
                            block.getWorld().getNearbyEntities(block.getLocation(), 10, 10, 10).forEach(entity -> {
                                if (entity instanceof Player) {
                                    ((Player) entity).sendMessage("§3WarfareDispenser §8» §7Dispenser na souřadnicích §fX " + block.getLocation().getX() + " Y "
                                            + block.getLocation().getY() + " Z " + block.getLocation().getZ() + " §7obsahuje více jak jeden Pickaxe. V tomto Dispenseru může být jen jeden Pickaxe.");
                                }
                            });
                            return;
                        }
                    }
                    if (blockNext.getType() != Material.BEDROCK) {
                        if (blockNext.getType().isSolid()) {
                            blockNext.breakNaturally();
                            Bukkit.getServer().getScheduler().runTaskLater(Main.getInstance(), () -> {
                                ItemStack item = e.getItem();
                                Damageable meta = (Damageable) e.getItem().getItemMeta();
                                meta.setDamage(meta.getDamage() + 1);
                                item.setItemMeta((ItemMeta) meta);
                                for (int i = 0; i < 9; i++) {
                                    container.getInventory().setItem(i, new ItemStack(Material.AIR));
                                }
                                ItemStack meta1 = new ItemStack(Material.DIAMOND_PICKAXE);
                                if (meta1.getType().getMaxDurability() > meta.getDamage()) {
                                    container.getInventory().setItem(0, item);
                                } else
                                    block.getWorld().getNearbyEntities(block.getLocation(), 10, 10, 10).forEach(entity -> {
                                        if (entity instanceof Player) {
                                            ((Player) entity).sendMessage("§3WarfareDispenser §8» §7Item v Dispenseru §fX " + block.getLocation().getX() + " Y "
                                                    + block.getLocation().getY() + " Z " + block.getLocation().getZ() + " §7se rozbil.");
                                        }
                                    });
                            }, 1);
                        }
                    } else
                        block.getWorld().playSound(block.getLocation(), Sound.BLOCK_ANVIL_FALL, 1, 1);
                }
                if (e.getItem().getType().toString().toLowerCase().contains("sword")) {
                    e.setCancelled(true);
                    for(ItemStack it : container.getInventory().getContents())
                    {
                        if(it != null) {
                            block.getWorld().getNearbyEntities(block.getLocation(), 10, 10, 10).forEach(entity -> {
                                if (entity instanceof Player) {
                                    ((Player) entity).sendMessage("§3WarfareDispenser §8» §7Dispenser na souřadnicích §fX " + block.getLocation().getX() + " Y "
                                            + block.getLocation().getY() + " Z " + block.getLocation().getZ() + " §7obsahuje více jak jeden Pickaxe. V tomto Dispenseru může být jen jeden Pickaxe.");
                                }
                            });
                            return;
                        }
                        block.getWorld().getNearbyEntities(blockNext.getLocation(), 1, 1,1).forEach(entity -> {
                            if (!(entity instanceof Player) || !(entity instanceof ArmorStand) || !(entity instanceof Minecart || !(entity instanceof Boat))) {
                                ((LivingEntity)entity).damage(getAttackDamage(e.getItem()));

                            }
                        });
                    }
                    Bukkit.getServer().getScheduler().runTaskLater(Main.getInstance(), () -> { ItemStack item = e.getItem();
                        Damageable meta = (Damageable) e.getItem().getItemMeta();
                        meta.setDamage(meta.getDamage() + 1);
                        item.setItemMeta((ItemMeta) meta);
                        for(int i = 0; i < 9; i++) {
                            container.getInventory().setItem(i, new ItemStack(Material.AIR));
                        }
                        ItemStack meta1 = new ItemStack(e.getItem().getType());
                        if (meta1.getType().getMaxDurability() > meta.getDamage()) {
                            container.getInventory().setItem(0, item);
                        } else
                            block.getWorld().getNearbyEntities(block.getLocation(), 10, 10, 10).forEach(entity -> {
                                if (entity instanceof Player) {
                                    ((Player) entity).sendMessage("§3WarfareDispenser §8» §7Item v Dispenseru §fX " + block.getLocation().getX() + " Y "
                                            + block.getLocation().getY() + " Z " + block.getLocation().getZ() + " §7se rozbil.");
                                }
                            });
                    }, 1);
                }
            }
        }
    }
    public double getAttackDamage(ItemStack itemStack) {
        double attackDamage = 0;
        if (itemStack.getItemMeta() instanceof Damageable) {
            Damageable meta = (Damageable) itemStack.getItemMeta();
            attackDamage = meta.getDamage();
        }
        return attackDamage;
    }
}