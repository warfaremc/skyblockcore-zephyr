package cz.yusari.sb.listeners;

import cz.yusari.sb.Main;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Directional;
import org.bukkit.block.data.type.Switch;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;

public class TNTYeeterAnimations {

    public static void startAnimations() {
        Location tnt1 = new Location(Bukkit.getWorld("Spawn4"), -6, 127, -49);
        Location tnt2 = new Location(Bukkit.getWorld("Spawn4"), -6, 125, -45);
        Location tnt3 = new Location(Bukkit.getWorld("Spawn4"), -6, 128, -48);
        Location tnt4 = new Location(Bukkit.getWorld("Spawn4"), -6, 127, -47);
        Location tnt5 = new Location(Bukkit.getWorld("Spawn4"), -6, 126, -46);
        Location body = new Location(Bukkit.getWorld("Spawn4"), -6, 126, -49);
        new BukkitRunnable() {
            public void run() {
                tnt1.getBlock().setType(Material.AIR);
                tnt3.getBlock().setType(Material.TNT);
                BlockData blockData = body.getBlock().getBlockData();
                BlockData data = Bukkit.getServer().createBlockData("minecraft:grindstone[face=wall]");
                blockData.merge(data);
                body.getBlock().setBlockData(data);
                body.getBlock().getState().update();
                tnt2.getWorld().playSound(tnt2, Sound.ENTITY_EVOKER_PREPARE_WOLOLO, 1,1);
                new BukkitRunnable() {
                    public void run() {
                        tnt3.getBlock().setType(Material.AIR);
                        tnt4.getBlock().setType(Material.TNT);
                        new BukkitRunnable() {
                            public void run() {
                                tnt4.getBlock().setType(Material.AIR);
                                tnt5.getBlock().setType(Material.TNT);
                                new BukkitRunnable() {
                                    public void run() {
                                        tnt5.getBlock().setType(Material.AIR);
                                        tnt2.getBlock().setType(Material.TNT);
                                        tnt2.getWorld().playSound(tnt2, Sound.BLOCK_GRASS_PLACE, 1,1);
                                        new BukkitRunnable() {
                                            public void run() {
                                                tnt2.getBlock().setType(Material.AIR);
                                                tnt2.getWorld().spawnParticle(Particle.EXPLOSION_LARGE,tnt2.getX(), tnt2.getY(), tnt2.getZ(), 5, 1,1,1,0);
                                                tnt2.getWorld().playSound(tnt2, Sound.ENTITY_GENERIC_EXPLODE, 1,1);
                                                new BukkitRunnable() {
                                                    public void run() {
                                                        tnt1.getBlock().setType(Material.TNT);
                                                        BlockData blockData = body.getBlock().getBlockData();
                                                        BlockData data = Bukkit.getServer().createBlockData("minecraft:grindstone[face=ceiling]");
                                                        blockData.merge(data);
                                                        body.getBlock().setBlockData(data);
                                                        body.getBlock().getState().update();
                                                    }
                                                }.runTaskLater(Main.getInstance(), 40L);
                                            }
                                        }.runTaskLater(Main.getInstance(), 40L);
                                    }
                                }.runTaskLater(Main.getInstance(), 10L);
                            }
                        }.runTaskLater(Main.getInstance(), 10L);
                    }
                }.runTaskLater(Main.getInstance(), 10L);
            }
        }.runTaskLater(Main.getInstance(), 100L);
    }
}
