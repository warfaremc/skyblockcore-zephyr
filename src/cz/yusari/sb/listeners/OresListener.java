package cz.yusari.sb.listeners;

import cz.yusari.sb.Main;
import cz.yusari.sb.utils.ItemFactory;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.data.BlockData;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Random;

public class OresListener implements Listener {

    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        Player p = e.getPlayer();
        Block block = e.getBlock();
        Material mat = block.getType();
        if (p.getWorld().getName().equalsIgnoreCase("endmine")) {
            e.setCancelled(true);
            if (block.getType() == Material.PURPLE_CONCRETE_POWDER || block.getType() == Material.MAGENTA_CONCRETE_POWDER || block.getType() == Material.END_STONE || block.getType() == Material.EMERALD_ORE || block.getType() == Material.CHORUS_PLANT) {
                if (block.getType() == Material.PURPLE_CONCRETE_POWDER) {
                    ItemStack purplium = ItemFactory.create(Material.PURPLE_DYE, "§dPurplium Dust");
                    p.getInventory().addItem(purplium);
                    p.giveExp(10);
                    p.sendMessage("§d§lEND MINE §7+1 Purplium Dust");
                }
                if (block.getType() == Material.MAGENTA_CONCRETE_POWDER) {
                    ItemStack purplium = ItemFactory.create(Material.GUNPOWDER, "§9Enderium Dust");
                    p.getInventory().addItem(purplium);
                    p.giveExp(10);
                    p.sendMessage("§d§lEND MINE §7+1 Enderium Dust");
                }
            if (block.getType() == Material.END_STONE) {
                if (block.getLocation().add(0, 1, 0).getBlock().getType() == Material.CHORUS_PLANT) {
                    p.sendMessage("§cNelze zníčit blok pod Chorusem.");
                    return;
                }
                ItemStack purplium = new ItemStack(Material.END_STONE);
                p.getInventory().addItem(purplium);
                p.sendMessage("§d§lEND MINE §7+1 End Stone");
            }
            if (block.getType() == Material.EMERALD_ORE) {
                ItemStack purplium = new ItemStack(Material.EMERALD);
                p.getInventory().addItem(purplium);
                p.giveExp(e.getExpToDrop());
                p.sendMessage("§d§lEND MINE §7+1 Emerald");
            }
            if (block.getType() == Material.CHORUS_PLANT) {
                if (block.getLocation().add(0, -1, 0).getBlock().getType() == Material.END_STONE) {
                    ItemStack purplium = new ItemStack(Material.CHORUS_FRUIT);
                    p.getInventory().addItem(purplium);
                    p.sendMessage("§d§lEND MINE §7+1 Chorus Fruit");
                } else {
                    p.sendMessage("§d§lEND MINE §7Musíš zníčit začátek Chorusu");
                    return;
                }
            }
                block.getWorld().spawnParticle(Particle.DRAGON_BREATH, block.getLocation().getX(), block.getLocation().getY(), block.getLocation().getZ(), 50, 0.5, 0.5, 0.5, 0);
                p.playSound(p.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
            BlockData data = block.getBlockData();
            block.setType(Material.BEDROCK);
            new BukkitRunnable() {
                public void run() {
                    if (mat == Material.CHORUS_PLANT) {
                        block.setType(Material.CHORUS_FLOWER);
                    } else {
                        block.setType(mat);
                        block.setBlockData(data);
                    }
                    block.getWorld().playSound(block.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, 1, 2);
                    block.getWorld().spawnParticle(Particle.PORTAL, block.getLocation().getX(), block.getLocation().getY(), block.getLocation().getZ(), 100, 1, 1, 1, 0);
                }
            }.runTaskLater(Main.getInstance(), 45 * 20);
        }
        }
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        if (e.getEntity().getWorld().getName().equalsIgnoreCase("endmine")) {
            if (e.getEntity().getLastDamageCause().getCause() == EntityDamageEvent.DamageCause.DROWNING) {
                for (Player p  : e.getEntity().getWorld().getPlayers()) {
                    p.sendMessage("§d§lEND MINE §e" + e.getEntity().getName() + " §7zemřel na nedostatek vzduchu.");
                }
            }
            if (e.getEntity().getLastDamageCause().getCause() == EntityDamageEvent.DamageCause.VOID) {
                for (Player p  : e.getEntity().getWorld().getPlayers()) {
                    p.sendMessage("§d§lEND MINE §e" + e.getEntity().getName() + " §7spáchal nežití.");
                }
            }
            if (e.getEntity().getLastDamageCause().getCause() == EntityDamageEvent.DamageCause.PROJECTILE) {
                for (Player p  : e.getEntity().getWorld().getPlayers()) {
                    p.sendMessage("§d§lEND MINE §e" + e.getEntity().getName() + " §7byl sestřelen.");
                }
            }
            if (e.getEntity().getLastDamageCause().getCause() == EntityDamageEvent.DamageCause.FIRE) {
                for (Player p  : e.getEntity().getWorld().getPlayers()) {
                    p.sendMessage("§d§lEND MINE §e" + e.getEntity().getName() + " §7uhořel.");
                }
            }
            if (e.getEntity().getLastDamageCause().getCause() == EntityDamageEvent.DamageCause.ENTITY_ATTACK) {
                for (Player p  : e.getEntity().getWorld().getPlayers()) {
                    p.sendMessage("§d§lEND MINE §e" + e.getEntity().getName() + " §7byl zabit hráčem.");
                }
            }
            if (e.getEntity().getLastDamageCause().getCause() == EntityDamageEvent.DamageCause.ENTITY_SWEEP_ATTACK) {
                for (Player p  : e.getEntity().getWorld().getPlayers()) {
                    p.sendMessage("§d§lEND MINE §e" + e.getEntity().getName() + " §7okusil ostříž meče.");
                }
            }
            if (e.getEntity().getLastDamageCause().getCause() == EntityDamageEvent.DamageCause.FALL) {
                for (Player p  : e.getEntity().getWorld().getPlayers()) {
                    p.sendMessage("§d§lEND MINE §e" + e.getEntity().getName() + " §7zakopl a spadl.");
                }
            }
            if (e.getEntity().getLastDamageCause().getCause() == EntityDamageEvent.DamageCause.SUFFOCATION) {
                for (Player p  : e.getEntity().getWorld().getPlayers()) {
                    p.sendMessage("§d§lEND MINE §e" + e.getEntity().getName() + " §7narazil s Elytrou.");
                }
            }
            if (e.getEntity().getLastDamageCause().getCause() == EntityDamageEvent.DamageCause.THORNS) {
                for (Player p  : e.getEntity().getWorld().getPlayers()) {
                    p.sendMessage("§d§lEND MINE §e" + e.getEntity().getName() + " §7zemřel na protivníkův armor.");
                }
            }
            if (e.getEntity().getLastDamageCause().getCause() == EntityDamageEvent.DamageCause.MAGIC) {
                for (Player p  : e.getEntity().getWorld().getPlayers()) {
                    p.sendMessage("§d§lEND MINE §e" + e.getEntity().getName() + " §7zemřel na magický efekt.");
                }
            }
        }
    }
}
