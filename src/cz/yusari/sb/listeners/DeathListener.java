package cz.yusari.sb.listeners;

import cz.yusari.sb.Main;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import java.util.HashMap;

public class DeathListener implements Listener {
    HashMap<Player, Integer> cache = new HashMap<>();
    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        Player p = e.getEntity();
        if (p.getFoodLevel() > 8) {
            cache.put(p, p.getFoodLevel());
        } else
            cache.put(p, 8);

    }
    @EventHandler
    public void onRespawn(PlayerRespawnEvent e) {
        if (cache.containsKey(e.getPlayer())) {
            Bukkit.getServer().getScheduler().runTaskLater(Main.getInstance(),  () -> {
                e.getPlayer().setFoodLevel(cache.get(e.getPlayer()));
                cache.remove(e.getPlayer());
            }, 5);
        }
    }
}
