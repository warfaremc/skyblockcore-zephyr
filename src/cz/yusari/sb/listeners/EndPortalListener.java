package cz.yusari.sb.listeners;

import cz.yusari.sb.Main;
import cz.yusari.sb.gui.FastTravelMenu;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.type.EndPortalFrame;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.DoubleChestInventory;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitScheduler;

import java.util.ArrayList;
import java.util.List;

public class EndPortalListener implements Listener {
    ArrayList<Player> cooldown = new ArrayList<>();
    ArrayList<Player> cooldowneg = new ArrayList<>();

    @EventHandler
    public void onPlace(BlockPlaceEvent e) {
        if (e.isCancelled() || !e.canBuild()) {
            return;
        }
        Player p = e.getPlayer();
        if (e.getBlock().getType() == Material.END_PORTAL_FRAME) {
            if (e.getItemInHand().hasItemMeta()) {
                if (e.getItemInHand().getItemMeta().hasDisplayName()) {
                    if (e.getItemInHand().getItemMeta().hasLore()) {
                        if (!isSafe(e.getBlock().getLocation(), 2)) {
                            p.sendMessage("§cVyčisti místo na vygenerování End Portalu");
                            e.setCancelled(true);
                            return;
                        }
                        e.getBlock().setType(Material.AIR);
                        generatePortal(p, e.getBlock().getLocation(), 2);
                    }
                }
            }
        }
    }

    public void generatePortal(Player p, Location loc, int radius) {
        setGround(loc, radius);
    }

    public boolean isSafe(Location loc, Integer radius) {
        final List<Location> locs = new ArrayList<>();

        final int startX = loc.getBlockX() - radius;
        final int endX = loc.getBlockX() + radius;

        final int startZ = loc.getBlockZ() - radius;
        final int endZ = loc.getBlockZ() + radius;

        for (int x = startX; x < endX; x++) {
            for (int z = startZ; z < endZ; z++) {
                if (x == startX || x == endX || z == startZ || z == endZ) {
                    Location l = new Location(loc.getWorld(), x, loc.getBlockY(), z);
                    locs.add(l);
                }
            }
        }

        for (int x = endX; x > startX; x--) {
            for (int z = endZ; z > startZ; z--) {
                if (x == startX || x == endX || z == startZ || z == endZ) {
                    Location l = new Location(loc.getWorld(), x, loc.getBlockY(), z);
                    locs.add(l);
                }
            }
        }
        for (Location locc : locs) {
            Location cord = new Location(locc.getWorld(), locc.getX(), locc.getY(), locc.getZ());
            Block block = cord.getBlock();
            if (block.getType() != Material.AIR) {
                return false;
            }
        }
        return true;
    }

    public void setGround(Location loc, Integer radius) {
        final List<Location> locs = new ArrayList<>();

        int i = 0;
        final int startX = loc.getBlockX() - radius;
        final int endX = loc.getBlockX() + radius;

        final int startZ = loc.getBlockZ() - radius;
        final int endZ = loc.getBlockZ() + radius;

        for (int x = startX; x < endX; x++) {
            for (int z = startZ; z < endZ; z++) {
                if (x == startX || x == endX || z == startZ || z == endZ) {
                    Location l = new Location(loc.getWorld(), x, loc.getBlockY(), z);
                    locs.add(l);
                }
            }
        }

        for (int x = endX; x > startX; x--) {
            for (int z = endZ; z > startZ; z--) {
                if (x == startX || x == endX || z == startZ || z == endZ) {
                    Location l = new Location(loc.getWorld(), x, loc.getBlockY(), z);
                    locs.add(l);
                }
            }
        }
        for (Location locc : locs) {
            i = i + 20;
            Location finalLoc = locc;
            new BukkitRunnable() {
                public void run() {
                    Location cord = new Location(finalLoc.getWorld(), finalLoc.getX(), finalLoc.getY(), finalLoc.getZ());
                    Block block = cord.getBlock();
                    block.setType(Material.END_PORTAL_FRAME);
                    block.getWorld().spawnParticle(Particle.FIREWORKS_SPARK, block.getLocation().getX(), block.getLocation().getY(), block.getLocation().getZ(), 50, 1, 1, 1, 0);
                    block.getWorld().playSound(block.getLocation(), Sound.ENTITY_CHICKEN_EGG, 1, 1);
                }
            }.runTaskLater(Main.getInstance(), i);
        }
        openPortal(loc);
    }

    /*    public void setGroundWithEye(Location loc, Integer radius) {
            final List<Location> locs = new ArrayList<>();

            int i = 340;
            final int startX = loc.getBlockX() - radius;
            final int endX = loc.getBlockX() + radius;

            final int startZ = loc.getBlockZ() - radius;
            final int endZ = loc.getBlockZ() + radius;

            for (int x = startX; x < endX; x++) {
                for (int z = startZ; z < endZ; z++) {
                    if (x == startX || x == endX || z == startZ || z == endZ) {
                        Location l = new Location(loc.getWorld(), x, loc.getBlockY(), z);
                        locs.add(l);
                    }
                }
            }

            for (int x = endX; x > startX; x--) {
                for (int z = endZ; z > startZ; z--) {
                    if (x == startX || x == endX || z == startZ || z == endZ) {
                        Location l = new Location(loc.getWorld(), x, loc.getBlockY(), z);
                        locs.add(l);
                    }
                }
            }
            for (Location locc : locs) {
                i = i + 20;
                Location finalLoc = locc;
                    new BukkitRunnable() {
                        public void run() {
                            Location cord = new Location(finalLoc.getWorld(), finalLoc.getX(), finalLoc.getY(), finalLoc.getZ());
                            Block block = cord.getBlock();
                            if (block.getType() != Material.END_PORTAL_FRAME) {
                                return;
                            }
                            ((EndPortalFrame)block.getBlockData()).setEye(true);
                            block.getWorld().spawnParticle(Particle.PORTAL, block.getLocation().getX(), block.getLocation().getY(), block.getLocation().getZ(), 50, 1, 1, 1, 0);
                            block.getWorld().playSound(block.getLocation(), Sound.ENTITY_HORSE_ARMOR, 1, 1);
                        }
                    }.runTaskLater(Main.getInstance(), i);
                }
            openPortal(loc);
        }*/
    public void openPortal(Location center) {
        new BukkitRunnable() {
            public void run() {
                center.getBlock().setType(Material.END_PORTAL);
                center.getBlock().getRelative(BlockFace.EAST).setType(Material.END_PORTAL);
                center.getBlock().getRelative(BlockFace.WEST).setType(Material.END_PORTAL);
                center.getBlock().getRelative(BlockFace.NORTH).setType(Material.END_PORTAL);
                center.getBlock().getRelative(BlockFace.SOUTH).setType(Material.END_PORTAL);
                center.getBlock().getRelative(BlockFace.SOUTH_EAST).setType(Material.END_PORTAL);
                center.getBlock().getRelative(BlockFace.SOUTH_WEST).setType(Material.END_PORTAL);
                center.getBlock().getRelative(BlockFace.NORTH_WEST).setType(Material.END_PORTAL);
                center.getBlock().getRelative(BlockFace.NORTH_EAST).setType(Material.END_PORTAL);
                center.getWorld().playSound(center, Sound.BLOCK_END_PORTAL_SPAWN, 1, 1);
                center.getWorld().playSound(center, Sound.ENTITY_ENDER_DRAGON_GROWL, 1, 1);
                center.getWorld().playSound(center, Sound.ENTITY_ENDER_DRAGON_FLAP, 1, 1);
            }
        }.runTaskLater(Main.getInstance(), 340);
    }

    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        Player p = e.getPlayer();
        if (cooldown.contains(p)) {
            return;
        }
        if (p.getLocation().getBlock().getType() == Material.END_PORTAL) {
            if (p.getLocation().getWorld().getName().equalsIgnoreCase("iridiumskyblock") || p.getLocation().getWorld().getName().equalsIgnoreCase("endmine")) {
                if (!(p.getOpenInventory() instanceof DoubleChestInventory)) {
                    p.sendMessage("§8§m----------------------------------------\n§3§lENDPORTAL  §7Pokud chceš do jiných dimenzí, klikni pravým na Portál (Ten černý blok s tečkama)\n§8§m----------------------------------------");
                    cooldown.add(p);
                    new BukkitRunnable() {
                        public void run() {
                            cooldown.remove(p);
                        }
                    }.runTaskLater(Main.getInstance(), 120L);
                }
            }
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        if (p.getLocation().getWorld().getName().equals("Spawn4")) {
            if (e.getClickedBlock() != null) {
                if (e.getClickedBlock().getType() == Material.STONE_BUTTON) {
                    if (e.getClickedBlock().getLocation().getX() == -15) {
                        if (e.getClickedBlock().getLocation().getY() == 125) {
                            if (e.getClickedBlock().getLocation().getZ() == -41) {
                                if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
                                    if (cooldowneg.isEmpty()) {
                                        TNTYeeterAnimations.startAnimations();
                                        cooldowneg.add(p);
                                        new BukkitRunnable() {
                                            public void run() {
                                                if (cooldowneg.remove(p)) ;
                                            }
                                        }.runTaskLater(Main.getInstance(), 350L);
                                    } else
                                        p.sendMessage("§3§lSERVER §7Tento EasterEgg se právě nabíjí.");
                                }
                            }
                        }
                    }
                } else {
                    if (e.getClickedBlock().getType() == Material.SKELETON_WALL_SKULL) {
                        if (e.getClickedBlock().getLocation().getX() == -6) {
                            if (e.getClickedBlock().getLocation().getY() == 126) {
                                if (e.getClickedBlock().getLocation().getZ() == -48) {
                                    p.sendMessage("§eTNT Yeeter §8» §fNajdi moje tajné tlačítko a ukážu ti trik.");
                                }
                            }
                        }
                    }
                }
            }
            } else if (p.getLocation().getWorld().getName().equalsIgnoreCase("iridiumskyblock") || p.getLocation().getWorld().getName().equalsIgnoreCase("endmine")) {
                if (e.getClickedBlock() != null) {
                    if (e.getClickedBlock().getType() == Material.END_PORTAL) {
                        FastTravelMenu.openParticlesMenu(p);
                    }
                }
            }
        }
    }
