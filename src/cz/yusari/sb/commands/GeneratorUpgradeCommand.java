package cz.yusari.sb.commands;

import cz.yusari.sb.upgrades.GeneratorUpgradeMenu;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GeneratorUpgradeCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String arg, String[] args) {
        if (sender instanceof Player) {
                GeneratorUpgradeMenu.openUpgradeMenu((Player) sender);
        }
        return false;
    }
}
