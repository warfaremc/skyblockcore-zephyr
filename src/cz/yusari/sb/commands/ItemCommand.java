package cz.yusari.sb.commands;

import cz.yusari.sb.listeners.TNTYeeterAnimations;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class ItemCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            if (p.hasPermission("core.item")) {
                ItemStack is = new ItemStack(Material.END_PORTAL_FRAME);
                ItemMeta im = is.getItemMeta();
                im.setDisplayName("§3End Portal Builder");
                List<String> lore = new ArrayList<String>();
                lore.add("§7Tento předmět vygeneruje");
                lore.add("§7portál do §dThe End Arény");
                im.setLore(lore);
                is.setItemMeta(im);
                p.getInventory().addItem(is);
            }
            if (args[0].equals("animate")) {
                if (p.getName().equals("sad_mirai")) {
                    TNTYeeterAnimations.startAnimations();
                }
            }
        }
        return false;
    }
}
