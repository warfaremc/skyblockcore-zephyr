package cz.yusari.sb.commands;

import cz.yusari.sb.blacksmith.BSMain;
import cz.yusari.sb.upgrades.GeneratorUpgradeMenu;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ForgeMenuCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String arg, String[] args) {
        if (sender instanceof Player) {
                BSMain.openForgeMenu((Player) sender);
        }
        return false;
    }
}
