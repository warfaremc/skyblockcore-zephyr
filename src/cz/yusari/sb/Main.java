package cz.yusari.sb;

import cz.yusari.sb.commands.ItemCommand;
import cz.yusari.sb.gui.FastTravelMenu;
import cz.yusari.sb.listeners.*;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Logger;

public class Main extends JavaPlugin {
    private static Main instance = null;
    private static Economy econ = null;
    private static Permission perms = null;
    private static Chat chat = null;
    private static final Logger log = Logger.getLogger("Minecraft");

    public void onEnable() {
        registerEvents();
        registerCommands();
        instance = this;

        if (!setupEconomy() ) {
            log.severe(String.format("[%s] - Disabled due to no Vault dependency found!", getDescription().getName()));
            getServer().getPluginManager().disablePlugin(this);
            return;
        }
        setupPermissions();
        setupChat();
    }

    public void registerEvents() {
        Bukkit.getServer().getPluginManager().registerEvents(new EndPortalListener(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new FastTravelMenu(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new CropsHarvest(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new OresListener(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new DeathListener(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new OreRegenListener(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new DispenserListener(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new AlternativeCommands(), this);
        Bukkit.broadcastMessage("§3§lSERVER §7SkyBlockCore v1.3.1-SNAPSHOT 1.15.2 zapnut!");
    }
    public void registerCommands() {
        getCommand("getitem").setExecutor(new ItemCommand());
    }
    public static Main getInstance() {
        return instance;
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    private boolean setupChat() {
        RegisteredServiceProvider<Chat> rsp = getServer().getServicesManager().getRegistration(Chat.class);
        chat = rsp.getProvider();
        return chat != null;
    }

    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> rsp = getServer().getServicesManager().getRegistration(Permission.class);
        perms = rsp.getProvider();
        return perms != null;
    }

    public static Economy getEconomy() {
        return econ;
    }

    public static Permission getPermissions() {
        return perms;
    }

    public static Chat getChat() {
        return chat;
    }
}
