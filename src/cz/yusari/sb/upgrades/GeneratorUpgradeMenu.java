package cz.yusari.sb.upgrades;

import cz.yusari.sb.Main;
import cz.yusari.sb.utils.ItemFactory;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class GeneratorUpgradeMenu implements Listener {

    public static void openUpgradeMenu(final Player p) {

        Inventory inv = Bukkit.createInventory(null, 9, "§3§lGenerator Upgrade");

        if (!p.hasPermission("oregen.upgrade.1")) {
            ItemStack i = ItemFactory.create(Material.COAL_BLOCK, "§a§lAdvanced Generator", "§7Spawnrate:", "§8▪ §aStone §757%", "§8▪ §aIro Ore §75%",
                    "§8▪ §aSandstone §75%", "§8▪ §aDiamond Ore §75%", "§8▪ §aGold Ore §75%", "§8▪ §aCoal Ore §710%", "§8▪ §aLapis Ore §75%", "§8▪ §aEmerald Ore §73%", "§8▪ §aStone Bricks §72%", "§aKliknutím zakoupíš za 100,000$");
            inv.setItem(0, i);
        } else if (p.hasPermission("oregen.upgrade.1")) {
            ItemStack i = ItemFactory.create(Material.EMERALD_BLOCK, "§a§lAdvanced Generator", "§7Tento Upgrade máš již zakoupený!");
            i = ItemFactory.addGlow(i);
            inv.setItem(0, i);
        }

        p.openInventory(inv);
    }

    @EventHandler
    private void onDrop(PlayerDropItemEvent e) {
        final Player p = e.getPlayer();
        if (p.getOpenInventory().getTitle().equals("§3§lGenerator Upgrade")) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    private void onInteract(InventoryClickEvent e) {
        final Player p = (Player) e.getWhoClicked();
        if (e.getView().getTitle().equals("§3§lGenerator Upgrade")) {
            e.setCancelled(true);
            if (e.getCurrentItem() == null) {
                return;
            }
            if (e.getCurrentItem().getType() == Material.AIR) {
                return;
            }
            if (e.getSlot() == 0) {
                if (!p.hasPermission("oregen.upgrade.1")) {
                    if (Main.getInstance().getEconomy().getBalance(p) > 100000) {
                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "lp user " + p.getName() + " perm set oregen.upgrade.1");
                        p.sendMessage("§aUpgrade Generátoru zakoupen.");
                        p.playSound(p.getLocation(), Sound.UI_TOAST_CHALLENGE_COMPLETE, 1, 1);
                        Main.getInstance().getEconomy().withdrawPlayer(p, 100000);
                        p.closeInventory();
                        return;
                    } else {
                        double needed = 100000 - Main.getInstance().getEconomy().getBalance(p);
                        p.sendMessage("§cNemáš požadovaný počet peněz. Chybí ti " + needed + "§a$");
                        p.closeInventory();
                        p.playSound(p.getLocation(), Sound.ENTITY_ITEM_BREAK, 1, (float) 0.5);
                        return;
                    }

                } else {
                    p.sendMessage("§cTento upgrade máš již zakoupený.");
                    return;
                }
            }

        }

    }
}
