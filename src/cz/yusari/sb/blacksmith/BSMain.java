package cz.yusari.sb.blacksmith;

import cz.yusari.sb.Main;
import cz.yusari.sb.utils.ItemFactory;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BSMain implements Listener {

    public static void openForgeMenu(final Player p) {

        Inventory inv = Bukkit.createInventory(null, 36, "§3Weapon Forge");

        for(int i = 0;i<36;i++) {
            ItemStack spacer = ItemFactory.create(Material.BLACK_STAINED_GLASS_PANE, "§8");
            inv.setItem(i, spacer);
        }
        ItemStack spacer = ItemFactory.create(Material.AIR, "");
        inv.setItem(19, spacer);
        inv.setItem(21, spacer);
        inv.setItem(23, spacer);
        ItemStack spacer1 = ItemFactory.create(Material.BARRIER, "§cZde bude výsledný item");
        inv.setItem(25, spacer1);
        ItemStack update = ItemFactory.create(Material.LIGHT_BLUE_STAINED_GLASS_PANE, "§3Aktualizovat", "§7Kliknutím aktualizuješ výsledný item");
        inv.setItem(27, update);
        ItemStack confirm = ItemFactory.create(Material.LIME_STAINED_GLASS_PANE, "§aPotvrdit", "§7Potvrdíš výsledný item");
        inv.setItem(35, confirm);
        ItemStack info1 = ItemFactory.create(Material.YELLOW_STAINED_GLASS_PANE, "§ePrvní item", "§7První item, který chceš sloučit", "§c§l(!) §cItem musí být stejného materiálu", "§a§l! JMÉNO TOHOTO JE VE VÝSLEDKU ZACHOVÁNO");
        inv.setItem(10, info1);
        ItemStack info2 = ItemFactory.create(Material.YELLOW_STAINED_GLASS_PANE, "§eDruhý item", "§7Druhý item, který chceš sloučit", "§c§l(!) §cItem musí být stejného materiálu", "§4§l!! JMÉNO ITEMU BUDE ZTRACENO");
        inv.setItem(12, info2);
        ItemStack info3 = ItemFactory.create(Material.YELLOW_STAINED_GLASS_PANE, "§eForgelin Stone §7(32x)", "§7Item, který dropuje z Forgelin Dwarfů", "§c§l(!) §cDosažitelné v Deadman's Cave");
        inv.setItem(14, info3);
        ItemStack info4 = ItemFactory.create(Material.YELLOW_STAINED_GLASS_PANE, "§eVýsledný item", "§7Item, který získáš po potvrzení", "§7Cena v počtu Money a XP se zobrazí v itemu");
        inv.setItem(16, info4);

        p.openInventory(inv);
    }
    @EventHandler
    private void onDrop(PlayerDropItemEvent e) {
        final Player p = e.getPlayer();
        if (p.getOpenInventory().getTitle().equals("§3Weapon Forge")) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    private void onInteract(InventoryClickEvent e) {
        final Player p = (Player) e.getWhoClicked();
        if (e.getView().getTitle().equals("§3Weapon Forge")) {
            if (e.getClickedInventory() == p.getInventory()) {
                return;
            }
            if (e.getCurrentItem() != null && e.getCurrentItem().getType() == Material.MAGENTA_STAINED_GLASS_PANE) {
                e.setCancelled(true);
                return;
            }
            if (e.getSlot() == 19 || e.getSlot() == 21 || e.getSlot() == 23) {
                if (e.getSlot() == 19 && e.getCursor() != null) {
                    if (e.getCursor().hasItemMeta()) {
                        if (e.getCursor().getItemMeta().hasLore()) {
                            if (e.getCursor().getItemMeta().getLore().contains("§9Item forged")) {
                                p.sendMessage("§cItem byl již jednou spojen a nelze ho spojit znovu.");
                                e.setCancelled(true);
                                return;
                            }
                        }
                    }
                    if (e.getClickedInventory().getItem(21) != null) {
                        if (e.getCursor().getType() == e.getClickedInventory().getItem(21).getType()) {
                            e.getClickedInventory().setItem(25, combinedItem(e.getCursor(), e.getClickedInventory().getItem(21)));
                            return;
                        }
                    }
                    return;
                }
                if (e.getSlot() == 21 && e.getCursor() != null) {
                    if (e.getCursor().hasItemMeta()) {
                        if (e.getCursor().getItemMeta().hasLore()) {
                            if (e.getCursor().getItemMeta().getLore().contains("§9Item forged")) {
                                p.sendMessage("§cItem byl již jednou spojen a nelze ho spojit znovu.");
                                e.setCancelled(true);
                                return;
                            }
                        }
                    }
                    if (e.getClickedInventory().getItem(19) != null) {
                        if (e.getCursor().getType() == e.getClickedInventory().getItem(19).getType()) {
                            e.getClickedInventory().setItem(25, combinedItem(e.getClickedInventory().getItem(19), e.getCursor()));
                            return;
                        }
                    }
                    return;
                }
                if (e.getSlot() == 23 && e.getCursor() != null) {
                    if (e.getCursor().getType() == Material.COAL) {
                        if (e.getCursor().hasItemMeta()) {
                            if (e.getCursor().getItemMeta().hasDisplayName()) {
                                if (e.getCursor().getItemMeta().getDisplayName().equals("§eForgelin Stone")) {
                                    if (e.getCursor().getItemMeta().hasLore()) {
                                        return;
                                    }
                                }
                            }
                        }
                    }
                    p.sendMessage("§cDaný item není §eForgelin Stone§c.");
                }
                if (e.getCurrentItem() != null) {
                    return;
                }
            }
            e.setCancelled(true);
            if (e.getCurrentItem() == null) {
                return;
            }
            if (e.getCurrentItem().getType() == Material.AIR) {
                return;
            }
            if (e.getSlot() == 27) {
                if (e.getClickedInventory().getItem(19) != null && e.getClickedInventory().getItem(21) != null) {
                    e.getInventory().setItem(25, combinedItem(e.getClickedInventory().getItem(19), e.getClickedInventory().getItem(21)));
                } else
                    p.sendMessage("§cPrvní item nebo druhý item je prázdný!");
            }
            if (e.getSlot() == 35) {
                if (e.getClickedInventory().getItem(19) == null) {
                    p.sendMessage("§cPrvní item je prázdný.");
                    return;
                }
                if (e.getClickedInventory().getItem(21) == null) {
                    p.sendMessage("§cDruhý item je prázdný.");
                    return;
                }
                if (e.getClickedInventory().getItem(23) == null) {
                    p.sendMessage("§cForgelin stone slot je prázdný.");
                    return;
                }
                if (!(e.getClickedInventory().getItem(23).getAmount() > 31)) {
                    p.sendMessage("§cNemáš dostatek Forgelin Stonů §7(32x)");
                    return;
                }
                if (e.getClickedInventory().getItem(23).getAmount() > 32) {
                    ItemStack forgelin = e.getClickedInventory().getItem(23);
                    forgelin.setAmount(e.getClickedInventory().getItem(23).getAmount()-32);
                    p.getInventory().addItem(forgelin);
                }
                p.playSound(p.getLocation(), Sound.BLOCK_ANVIL_USE, 1, 1);
                initAnimation(p, combinedItem(e.getClickedInventory().getItem(19), e.getClickedInventory().getItem(21)));
            }

        }

    }
    @EventHandler
    public void onClose(InventoryCloseEvent e) {
        Player p = (Player) e.getPlayer();
        if (e.getView().getTitle().equals("§3Weapon Forge")) {
            if (e.getInventory().getItem(23) != null) {
                if (e.getInventory().getItem(23).getType() == Material.MAGENTA_STAINED_GLASS_PANE) {
                    return;
                }
                ItemStack forgelin = e.getInventory().getItem(23);
                p.getInventory().addItem(forgelin);
        }
            if (e.getInventory().getItem(21).getType() == Material.AIR || e.getInventory().getItem(21) != null) {
                if (e.getInventory().getItem(21).getType() == Material.MAGENTA_STAINED_GLASS_PANE) {
                    return;
                }
                ItemStack second = e.getInventory().getItem(21);
                p.getInventory().addItem(second);
            }
            if (e.getInventory().getItem(19).getType() == Material.AIR || e.getInventory().getItem(19) != null) {
                if (e.getInventory().getItem(19).getType() == Material.MAGENTA_STAINED_GLASS_PANE) {
                    return;
                }
            ItemStack first = e.getInventory().getItem(19);
                p.getInventory().addItem(first);
            }
        }
    }
    public ItemStack combinedItem(ItemStack first, ItemStack second) {
        ItemStack item = first.clone();
            ItemMeta meta = item.getItemMeta();
            Map<Enchantment, Integer> enchants;
            enchants = second.getEnchantments();
        item.getEnchantments().forEach((enchantment, level) -> {
                if (enchants.containsKey(enchantment)) {
                    meta.removeEnchant(enchantment);
                    meta.addEnchant(enchantment, Math.round((second.getEnchantmentLevel(enchantment) + level)), true);
                } else
                    meta.addEnchant(enchantment, level, true);
            });
        second.getEnchantments().forEach((enchantment, level) -> {
            if (!meta.getEnchants().containsKey(enchantment)) {
                meta.addEnchant(enchantment, level, true);
            }
        });
        List<String> lore = new ArrayList<>();
            if (item.getItemMeta().hasLore()) {
                lore = meta.getLore();
                lore.add("§9Item forged");
            } else {
                lore.add("§9Item forged");
            }
            meta.setLore(lore);
            item.setItemMeta(meta);
        return item;
    }
    public void initAnimation(Player p, ItemStack item) {
                    p.playSound(p.getLocation(), Sound.UI_TOAST_CHALLENGE_COMPLETE, 1, 1);
                    p.getInventory().addItem(item);
                    p.closeInventory();
                }

                @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if (checkForged(e.getPlayer().getInventory().getItemInMainHand())) {
                        e.getPlayer().getInventory().setItemInMainHand(new ItemStack(Material.AIR));
                        e.getPlayer().sendMessage("§cItem Forged item odebrán.");
                    }
                    if (checkForged(e.getPlayer().getInventory().getItemInOffHand())) {
                        e.getPlayer().getInventory().setItemInOffHand(new ItemStack(Material.AIR));
                        e.getPlayer().sendMessage("§cItem Forged item odebrán.");
                    }
                }
                @EventHandler
                public void onInventoryClick(InventoryClickEvent e) {
        if (checkForged(e.getCurrentItem())) {
            e.getInventory().setItem(e.getSlot(), new ItemStack(Material.AIR));
            e.getWhoClicked().sendMessage("§cItem Forged item odebrán.");
                    }
                }
public boolean checkForged(ItemStack item) {
        if (item.hasItemMeta()) {
            if (item.getItemMeta().hasLore()) {
                if (item.getItemMeta().getLore().contains("§9Item forged")) {
                    return true;
                }
            }
        }
        return false;
}
        }
