package cz.yusari.sb.gui;


import cz.yusari.sb.utils.ItemFactory;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class FastTravelMenu implements Listener {

    private static int rows;

    public static void openParticlesMenu(final Player p) {

        Inventory inv = Bukkit.createInventory(null, 9, "§d§lFast Travel");

        for (int i = 0; i < 9; i++) {
            /*if (inv.getItem(i) != null || inv.getItem(i).getType() != Material.AIR) {
                return;
            }*/
            ItemStack nic = ItemFactory.create(Material.BLACK_STAINED_GLASS_PANE, "§8");
            inv.setItem(i, nic);
        }
        try {
            ItemStack survival = ItemFactory.create(Material.DRAGON_EGG, "§d§lCursed End", "§7", "§8PvE, Boss", "", "§aKliknutím Teleportuješ");
            inv.setItem(2, survival);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            ItemStack survival = ItemFactory.create(Material.END_STONE, "§5§lEnd Mine", "§7", "§8PvP, Těžení", "", "§aKliknutím Teleportuješ");
            inv.setItem(6, survival);
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        p.openInventory(inv);
    }

    @EventHandler
    private void onDrop(PlayerDropItemEvent e) {
        final Player p = e.getPlayer();
        if (p.getOpenInventory().getTitle().equals("§d§lFast Travel")) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    private void onInteract(InventoryClickEvent e) {
        final Player p = (Player) e.getWhoClicked();
        if (e.getView().getTitle().equals("§d§lFast Travel")) {
            e.setCancelled(true);
            if (e.getCurrentItem() == null) {
                return;
            }
            if (e.getCurrentItem().getType() == Material.AIR) {
                return;
            }
            if (e.getSlot() == 2) {
                Location loc = new Location(Bukkit.getWorld("EndArena"), -111, 60, -8);
                p.teleport(loc);
                p.sendMessage("§8§m---§f §d§lScottyho Fast Travel §8§m---\n" +
                        "§7Děkujeme za použití Fast Travelu.\n" +
                        "§7Byl jste teleportován na destinaci §dCursed End§7.");
            }
            if (e.getSlot() == 6) {
                Location loc = new Location(Bukkit.getWorld("EndMine"), -83.5, 61, 84.5);
                p.teleport(loc);
                p.sendMessage("§8§m---§f §d§lScottyho Fast Travel §8§m---\n" +
                        "§7Děkujeme za použití Fast Travelu.\n" +
                        "§7Byl jste teleportován na destinaci §5End Mine§7.");
            }
        }

    }
}
